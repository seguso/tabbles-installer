﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace ShellMenu.Interop.Shell
{
	/// <summary>IShellExtInit Shell interface.</summary>
	[ComImport, Guid("000214E8-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IShellExtInit
	{
		void Initialize(IntPtr pidlFolder, IDataObject dtObj, IntPtr hKeyProgId);
	}
}