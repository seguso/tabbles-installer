﻿using System;
using System.Runtime.InteropServices;
using FileOperation.Interop;
using FileOperation.Registration;

namespace FileOperation
{
	[ComVisible(true), Guid("6E55D874-7A61-4333-B431-C33D4C777487"), ClassInterface(ClassInterfaceType.None)]
	public class NewFileOperation : ICustomQueryInterface
	{
		private readonly IFileOperation _original;

		public NewFileOperation()
		{
			Guid clsid = typeof (Interop.FileOperation).GUID;
			Guid iid = typeof (IFileOperation).GUID;
			_original = (IFileOperation) ComHelper.GetDllClassObject("shell32.dll", clsid, iid);

			if (ClientHelper.IsExplorerProcess())
			{
				var sink = new NewFileOperationSink();
				int cookie;
				_original.Advice(sink, out cookie);
			}
		}

		public CustomQueryInterfaceResult GetInterface(ref Guid iid, out IntPtr ppv)
		{
			if (iid == typeof (IFileOperation).GUID)
			{
				ppv = Marshal.GetComInterfaceForObject(_original, typeof (IFileOperation));
				return CustomQueryInterfaceResult.Handled;
			}

			ppv = IntPtr.Zero;
			return CustomQueryInterfaceResult.NotHandled;
		}

		[ComRegisterFunction]
		public static void Register(Type type)
		{
			if (Environment.OSVersion.Version.Major >= 6)
			{
				EmulationHelper.RegTreatAs(typeof (Interop.FileOperation).GUID, typeof (NewFileOperation).GUID);
			}
		}

		[ComUnregisterFunction]
		public static void Unregister(Type type)
		{
			if (Environment.OSVersion.Version.Major >= 6)
			{
				EmulationHelper.RegTreatAs(typeof (Interop.FileOperation).GUID, Guid.Empty);
			}
		}
	}
}