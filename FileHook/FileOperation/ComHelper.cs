using System;
using System.Runtime.InteropServices;
using FileOperation.Interop;

namespace FileOperation
{
	internal static class ComHelper
	{
		public delegate int DllGetClassObjectDelegate(
			[MarshalAs(UnmanagedType.LPStruct)] Guid rclsid, [MarshalAs(UnmanagedType.LPStruct)] Guid riid,
			[MarshalAs(UnmanagedType.IUnknown)] out object ppv);

		public static object GetDllClassObject(string dllName, Guid clsid, Guid iid)
		{
			IntPtr hModShell = NativeMethods.CoLoadLibrary(dllName, true);
			IntPtr getClassObjectPtr = NativeMethods.GetProcAddress(hModShell, "DllGetClassObject");
			if (getClassObjectPtr == IntPtr.Zero)
			{
				int hr = Marshal.GetHRForLastWin32Error();
				Marshal.ThrowExceptionForHR(hr);
			}

			var getClassObject =
				(DllGetClassObjectDelegate)
					Marshal.GetDelegateForFunctionPointer(getClassObjectPtr, typeof (DllGetClassObjectDelegate));
			object classFactoryUnk;
			int hr2 = getClassObject(clsid, typeof (IClassFactory).GUID, out classFactoryUnk);
			Marshal.ThrowExceptionForHR(hr2);

			var classFactory = (IClassFactory) classFactoryUnk;
			return classFactory.CreateInstance(IntPtr.Zero, iid);
		}
	}
}