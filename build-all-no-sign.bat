SET config=Release
SET msbuild="%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"

%msbuild% /m /p:Configuration=%config% "RegistrationManager\RegistrationManager.sln"
@IF NOT %ERRORLEVEL%==0 GOTO :EOF
%msbuild% /m /p:Configuration=%config% "TabblesShellMenu\TabblesShellMenu.sln"
@IF NOT %ERRORLEVEL%==0 GOTO :EOF
%msbuild% /m /p:Configuration=%config% "FileHook\FileOperation.sln"
@IF NOT %ERRORLEVEL%==0 GOTO :EOF
%msbuild% /m /p:Configuration=%config% "FileHook\PipeServer.sln"
@IF NOT %ERRORLEVEL%==0 GOTO :EOF
%msbuild% /m /p:Configuration=%config% "TabblesInstall\TabblesInstall.sln"
@IF NOT %ERRORLEVEL%==0 GOTO :EOF

XCOPY TabblesInstall\TabblesInstall\bin\%config%\*.* out\ /Y
