﻿using System;
using System.Diagnostics;
using Microsoft.Win32;

namespace RemoveTabbles
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			if (args.Length != 1)
			{
				Console.WriteLine("Version argument required.");
				return;
			}

			Version minVersion;
			if (!Version.TryParse(args[0], out minVersion))
			{
				return;
			}
			RemoveTabblesUpToVersion(minVersion);
		}

		private static void RemoveTabblesUpToVersion(Version minVersion)
		{
			using (RegistryKey tabbles = Registry.LocalMachine.OpenSubKey(@"Software\Yellow blue soft\Tabbles"))
			{
				if (tabbles == null)
				{
					return;
				}

				var versionStr = tabbles.GetValue("product_version") as string;
				if (versionStr == null)
				{
					return;
				}

				Version version;
				if (!Version.TryParse(versionStr, out version))
				{
					return;
				}

				if (version > minVersion)
				{
					return;
				}

				var productCode = tabbles.GetValue("product_code") as string;
				if (productCode == null)
				{
					return;
				}

				RemoveProduct(productCode);
			}
		}

		private static void RemoveProduct(string productCode)
		{
			string ars = string.Format("/x {0} /passive", productCode);

			using (Process proc = Process.Start("msiexec.exe", ars))
			{
				proc.WaitForExit();
			}
		}
	}
}