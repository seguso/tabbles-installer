﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using SharpShell.SharpIconOverlayHandler;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.IO.Pipes;
using OverlayHandlerCommon;

namespace TabblesOverlayHandler
{
    [ComVisible(true)]
    class TabblesOverlayHandler : SharpIconOverlayHandler
    {


        private const int idHandler = 1;


        private bool initialized = false;
        private Dictionary<string, bool> cache = new Dictionary<string,bool>();


        protected override bool CanShowOverlay(string path, SharpShell.Interop.FILE_ATTRIBUTE attributes)
        {
            return Common.CanShowOverlay(path, ref initialized, cache, idHandler);
        }

        protected override System.Drawing.Icon GetOverlayIcon()
        {

            Common.log("getoverlayicon called", idHandler);
            return Resource1.tabbles_overlay2;
        }

        protected override int GetPriority()
        {
            Common.log("getPriority called", idHandler);
            //  very low priority.
            return 90;
        }
    }
}
