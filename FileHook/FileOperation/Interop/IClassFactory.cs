﻿using System;
using System.Runtime.InteropServices;

namespace FileOperation.Interop
{
	[ComImport, Guid("00000001-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IClassFactory
	{
		[return: MarshalAs(UnmanagedType.IUnknown)]
		object CreateInstance(IntPtr pUnkOuter, [MarshalAs(UnmanagedType.LPStruct)] Guid riid);

		void LockServer(bool fLock);
	}
}