﻿using System;

namespace ShellMenu.Interop.Win
{
	/// <see cref="MenuItemInfo" />
	[Flags]
	internal enum MenuItemInfoMask
	{
		State = 0x01,
		Id = 0x02,
		SubMenu = 0x04,
		CheckMarks = 0x08,
		Type = 0x10,
		Data = 0x20,
		String = 0x40,
		Bitmap = 0x80,
		FType = 0x100
	}
}