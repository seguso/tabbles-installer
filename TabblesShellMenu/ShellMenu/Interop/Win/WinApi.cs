﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace ShellMenu.Interop.Win
{
	/// <summary>Wrapper of Windows API functions.</summary>
	internal abstract class WinApi
	{
		[DllImport("User32.dll", SetLastError = true)]
		public static extern bool InsertMenuItem(IntPtr hMenu, int item, bool byPosition, [In] ref MenuItemInfo mii);

		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern IntPtr GlobalLock(IntPtr hMem);

		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern bool GlobalUnlock(IntPtr hMem);

		[DllImport("Shell32.dll")]
		public static extern int DragQueryFile(IntPtr hDrop, int iFile, StringBuilder file, int cCh);

		[DllImport("Ole32.dll")]
		public static extern void ReleaseStgMedium(ref STGMEDIUM medium);

		[DllImport("User32.dll ", SetLastError = true)]
		public static extern bool GetMenuInfo(IntPtr hMenu, ref MenuInfo mi);

		[DllImport("User32.dll ", SetLastError = true)]
		public static extern bool SetMenuInfo(IntPtr hMenu, [In] ref MenuInfo mi);

		public static int MakeHResult(int sev, int fac, int code)
		{
			return sev << 31 | fac << 16 | code;
		}
	}
}