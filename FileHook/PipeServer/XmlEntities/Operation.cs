﻿using System.Xml.Serialization;

namespace PipeServer.XmlEntities
{
	[XmlRoot("operation")]
	public class Operation
	{
		[XmlAttribute("type")]
		public string Type { get; set; }

		[XmlAttribute("destination")]
		public string Destination { get; set; }

		[XmlElement("file")]
		public File[] Files { get; set; }
	}
}