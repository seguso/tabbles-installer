﻿using System;

namespace ShellMenu.Interop.Win
{
	/// <see cref="MenuInfo" />
	[Flags]
	internal enum MenuInfoMask
	{
		MaxHeight = 0x01,
		Background = 0x02,
		HelpId = 0x04,
		MenuData = 0x08,
		Style = 0x10,
		ApplyToSubmenus = unchecked ((int) 0x80000000)
	}
}