﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Win
{
	/// <summary>Windows MENUINFO structure.</summary>
	[StructLayout(LayoutKind.Sequential)]
	internal struct MenuInfo
	{
		public int Size;
		public MenuInfoMask Mask;
		public MenuInfoStyle Style;
		public int YMax;
		public IntPtr hBrBack;
		public int ContextHelpId;
		public IntPtr MenuData;
	}
}