﻿using System.Runtime.InteropServices;

namespace FileOperation.Registration.Interop
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct TokenPrivileges
	{
		public int PrivilegeCount;
		[MarshalAs(UnmanagedType.ByValArray)] public LuidAndAttributes[] Privileges;
	}
}