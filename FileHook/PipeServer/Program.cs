﻿using System;

namespace PipeServer
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var server = new Server();

			while (true)
			{
				string msg = server.Read();
				Console.WriteLine(msg);
				Console.WriteLine();
			}
		}
	}
}