using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace FileOperation.Interop
{
	[ComImport, Guid("43826d1e-e718-42ee-bc55-a1e261c37bfe"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface IShellItem
	{
		[PreserveSig]
		int BindToHandler(IBindCtx pbc, [MarshalAs(UnmanagedType.LPStruct)] Guid bhid,
			[MarshalAs(UnmanagedType.LPStruct)] Guid riid, out IntPtr ppv);

		void GetParent(out IShellItem ppsi);

		[return: MarshalAs(UnmanagedType.LPWStr)]
		string GetDisplayName(Sigdn sigdnName);

		void GetAttributes(int sfgaoMask, out int psfgaoAttribs);
		void Compare(IShellItem psi, int hint, out int piOrder);
	}
}