using System.Runtime.InteropServices;

namespace FileOperation.Interop
{
	[ComImport, Guid("04b0f1a7-9490-44bc-96e1-4296a31252e2"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface IFileOperationProgressSink
	{
		[PreserveSig]
		int StartOperations();

		[PreserveSig]
		int FinishOperations(int hrResult);

		[PreserveSig]
		int PreRenameItem(int dwFlags, IShellItem psiItem, [MarshalAs(UnmanagedType.LPWStr)] string pszNewName);

		[PreserveSig]
		int PostRenameItem(int dwFlags, IShellItem psiItem, [MarshalAs(UnmanagedType.LPWStr)] string pszNewName,
			int hrRename,
			IShellItem psiNewlyCreated);

		[PreserveSig]
		int PreMoveItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder,
			[MarshalAs(UnmanagedType.LPWStr)] string pszNewName);

		[PreserveSig]
		int PostMoveItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder,
			[MarshalAs(UnmanagedType.LPWStr)] string pszNewName, int hrMove, IShellItem psiNewlyCreated);

		[PreserveSig]
		int PreCopyItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder,
			[MarshalAs(UnmanagedType.LPWStr)] string pszNewName);

		[PreserveSig]
		int PostCopyItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder,
			[MarshalAs(UnmanagedType.LPWStr)] string pszNewName, int hrCopy, IShellItem psiNewlyCreated);

		[PreserveSig]
		int PreDeleteItem(int dwFlags, IShellItem psiItem);

		[PreserveSig]
		int PostDeleteItem(int dwFlags, IShellItem psiItem, int hrDelete, IShellItem psiNewlyCreated);

		[PreserveSig]
		int PreNewItem(int dwFlags, IShellItem psiDestinationFolder, [MarshalAs(UnmanagedType.LPWStr)] string pszNewName);

		[PreserveSig]
		int PostNewItem(int dwFlags, IShellItem psiDestinationFolder,
			[MarshalAs(UnmanagedType.LPWStr)] string pszNewName,
			[MarshalAs(UnmanagedType.LPWStr)] string pszTemplateName, int dwFileAttributes, int hrNew,
			IShellItem psiNewItem);

		[PreserveSig]
		int UpdateProgress(int iWorkTotal, int iWorkSoFar);

		[PreserveSig]
		int ResetTimer();

		[PreserveSig]
		int PauseTimer();

		[PreserveSig]
		int ResumeTimer();
	}
}