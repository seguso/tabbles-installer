﻿using System;
using System.Runtime.InteropServices;

namespace FileOperation.Interop
{
	[ComImport, Guid("947aab5f-0a5c-4c13-b4d6-4bf7836fc9f8"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IFileOperation
	{
		[PreserveSig]
		int Advice(IFileOperationProgressSink pfops, out int pdwCookie);

		[PreserveSig]
		int Unadvice(int dwCookie);

		[PreserveSig]
		int SetOperationFlags(int dwOperationFlags);

		[PreserveSig]
		int SetProgressMessage(IntPtr pszMessage);

		[PreserveSig]
		int SetProgressDialog(IntPtr popd);

		[PreserveSig]
		int SetProperties(IntPtr pproparray);

		[PreserveSig]
		int SetOwnerWindow(IntPtr hwndOwner);

		[PreserveSig]
		int ApplyPropertiesToItem(IntPtr psiItem);

		[PreserveSig]
		int ApplyPropertiesToItems(IntPtr punkItems);

		[PreserveSig]
		int RenameItem(IShellItem psiItem, IntPtr pszNewName, IFileOperationProgressSink pfopsItem);

		[PreserveSig]
		int RenameItems(IntPtr pUnkItems, IntPtr pszNewName);

		[PreserveSig]
		int MoveItem(IntPtr psiItem, IntPtr psiDestinationFolder, IntPtr pszNewName, IntPtr pfopsItem);

		[PreserveSig]
		int MoveItems(IntPtr punkItems, IntPtr psiDestinationFolder);

		[PreserveSig]
		int CopyItem(IntPtr psiItem, IntPtr psiDestinationFolder, IntPtr pszCopyName, IntPtr pfopsItem);

		[PreserveSig]
		int CopyItems(IntPtr punkItems, IntPtr psiDestinationFolder);

		[PreserveSig]
		int DeleteItem(IntPtr psiItem, IntPtr pfopsItem);

		[PreserveSig]
		int DeleteItems(IntPtr punkItems);

		[PreserveSig]
		int NewItem(IntPtr psiDestinationFolder, int dwFileAttributes, IntPtr pszName, IntPtr pszTemplateName,
			IntPtr pfopsItem);

		[PreserveSig]
		int PerformOperations();

		[PreserveSig]
		int GetAnyOperationsAborted(out bool pfAnyOperationsAborted);
	}
}