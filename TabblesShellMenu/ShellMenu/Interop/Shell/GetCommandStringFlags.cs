﻿namespace ShellMenu.Interop.Shell
{
	/// <summary>GetCommandString flags.</summary>
	/// <see cref="IContextMenu" />
	public enum GetCommandStringFlags
	{
		VerbA = 0,
		HelpTextA = 0x01,
		ValidateA = 0x02,
		VerbW = 0x04,
		HelpTextW = 0x05,
		ValidateW = 0x06,
		VerbIconW = 0x14,
		Unicide = 0x04
	}
}