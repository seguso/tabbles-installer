﻿using System;
using Microsoft.Win32;
using ShellMenu;

namespace TabblesShellMenu
{
	public abstract class RegistrationHelper : ContextMenuBase
	{
		private const string ApprovedPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved";
		private const string MenuPath = @"AllFilesystemObjects\shellex\ContextMenuHandlers";

		private static readonly string MenuGuid = typeof (ContextMenu).GUID.ToString("B");

		protected static void Register(Type type)
		{
			// Register menu.
			RegistryKey menuKey = Registry.ClassesRoot.CreateSubKey(MenuPath + @"\TabblesShellMenu");
			menuKey.SetValue(null, MenuGuid);

			// Add to approved
			RegistryKey approvedKey = Registry.LocalMachine.CreateSubKey(ApprovedPath);
			approvedKey.SetValue(MenuGuid, "Tabbles shell menu");
		}

		protected static void Unregister(Type type)
		{
			// Unregister menu.
			RegistryKey menuKey = Registry.ClassesRoot.OpenSubKey(MenuPath, true);
			if (menuKey != null)
			{
				try
				{
					menuKey.DeleteSubKey("TabblesShellMenu");
				}
				catch (ArgumentException)
				{
				}
			}

			// Remove from approved
			RegistryKey approvedKey = Registry.LocalMachine.OpenSubKey(ApprovedPath, true);
			if (approvedKey != null)
			{
				try
				{
					approvedKey.DeleteSubKey(MenuGuid);
				}
				catch (ArgumentException)
				{
				}
			}
		}
	}
}