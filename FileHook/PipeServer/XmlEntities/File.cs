﻿using System.Xml.Serialization;

namespace PipeServer.XmlEntities
{
	public class File
	{
		[XmlAttribute("path")]
		public string Path { get; set; }

		[XmlAttribute("newName")]
		public string NewName { get; set; }

		[XmlAttribute("hResult")]
		public int HResult { get; set; }
	}
}