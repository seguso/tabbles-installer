﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Win
{
	/// <summary>Windows DRAWITEMSTRUCT structure.</summary>
	[StructLayout(LayoutKind.Sequential)]
	internal struct DrawItemStruct
	{
		public int CtlType;
		public int CtlId;
		public int ItemId;
		public int ItemAction;
		public int ItemState;
		public IntPtr hWndItem;
		public IntPtr hDc;
		public Rect RcItem;
		public IntPtr ItemData;
	}
}