﻿using System;

namespace ShellMenu.Interop.Win
{
	/// <see cref="MenuInfo" />
	/// >
	[Flags]
	internal enum MenuInfoStyle
	{
		NoCheck = unchecked((int) 0x80000000),
		Modeless = 0x40000000,
		DragDrop = 0x20000000,
		AutoDismiss = 0x10000000,
		NotifyByPos = 0x08000000,
		CheckOrBmp = 0x04000000
	}
}