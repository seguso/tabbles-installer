using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileOperation.Interop;
using FileOperation.XmlEntities;
using File = FileOperation.XmlEntities.File;

namespace FileOperation
{
	internal class NewFileOperationSink : IFileOperationProgressSink
	{
		private const int SOk = 0;
		private readonly List<OperationInternal> _operations = new List<OperationInternal>();
		private Dictionary<string, bool> _deletedFileIsDir;

		public int StartOperations()
		{
			_deletedFileIsDir = new Dictionary<string, bool>();
			return SOk;
		}

		public int FinishOperations(int hrResult)
		{
			try
			{
				ILookup<string, OperationInternal> opTypes = _operations.ToLookup(x => x.Type);

				foreach (var opType in opTypes)
				{
					ILookup<string, OperationInternal> opGroups = opType.ToLookup(o => o.Destination);

					foreach (var opGroup in opGroups)
					{
						var operation = new Operation {Type = opType.Key, Destination = opGroup.Key};
						IEnumerable<File> files = (from f in opGroup
							select new File {HResult = f.HResult, Path = f.Path, NewName = f.NewName, IsDir = f.IsDir});
						operation.Files = files.ToArray();

						ClientHelper.Send(operation);
					}
				}
			}
			catch
			{
			}

			return SOk;
		}

		public int PreRenameItem(int dwFlags, IShellItem psiItem, string pszNewName)
		{
			return SOk;
		}

		public int PostRenameItem(int dwFlags, IShellItem psiItem, string pszNewName, int hrRename, IShellItem psiNewlyCreated)
		{
			try
			{
				string oldPath = psiItem.GetDisplayName(Sigdn.DesktopAbsoluteParsing);


				string dir = Path.GetDirectoryName(oldPath);
				string newPath = Path.Combine(dir, pszNewName);
				bool isDir = Directory.Exists(newPath);

				var op = new OperationInternal
				{
					Type = "rename",
					Path = oldPath,
					NewName = pszNewName,
					HResult = hrRename,
					IsDir = isDir
				};
				_operations.Add(op);
			}
			catch
			{
			}
			return SOk;
		}

		public int PreMoveItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder, string pszNewName)
		{
			return SOk;
		}

		public int PostMoveItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder, string pszNewName,
			int hrMove, IShellItem psiNewlyCreated)
		{
			try
			{
				string oldPath = psiItem.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
				string destFolder = psiDestinationFolder.GetDisplayName(Sigdn.DesktopAbsoluteParsing);


				string newPath = Path.Combine(destFolder, pszNewName);
				bool isDir = Directory.Exists(newPath);

				var op = new OperationInternal
				{
					Type = "move",
					Path = oldPath,
					Destination = destFolder,
					NewName = pszNewName,
					HResult = hrMove,
					IsDir = isDir
				};
				_operations.Add(op);
			}
			catch
			{
			}
			return SOk;
		}

		public int PreCopyItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder, string pszNewName)
		{
			return SOk;
		}

		public int PostCopyItem(int dwFlags, IShellItem psiItem, IShellItem psiDestinationFolder, string pszNewName,
			int hrCopy, IShellItem psiNewlyCreated)
		{
			try
			{
				string path = psiItem.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
				string dest = psiDestinationFolder.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
				var op = new OperationInternal
				{
					Type = "copy",
					Path = path,
					Destination = dest,
					NewName = pszNewName,
					HResult = hrCopy
				};
				_operations.Add(op);
			}
			catch
			{
			}
			return SOk;
		}

		public int PreDeleteItem(int dwFlags, IShellItem psiItem)
		{
			try
			{
				string path = psiItem.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
				bool isDir = Directory.Exists(path);
				_deletedFileIsDir[path] = isDir;
			}
			catch
			{
			}
			return SOk;
		}

		public int PostDeleteItem(int dwFlags, IShellItem psiItem, int hrDelete, IShellItem psiNewlyCreated)
		{
			try
			{
				string path = psiItem.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
				bool isDir = _deletedFileIsDir[path];
				_deletedFileIsDir.Remove(path);
				var op = new OperationInternal {Type = "delete", Path = path, HResult = hrDelete, IsDir = isDir};
				_operations.Add(op);
			}
			catch
			{
			}
			return SOk;
		}

		public int PreNewItem(int dwFlags, IShellItem psiDestinationFolder, string pszNewName)
		{
			return SOk;
		}

		public int PostNewItem(int dwFlags, IShellItem psiDestinationFolder, string pszNewName, string pszTemplateName,
			int dwFileAttributes, int hrNew, IShellItem psiNewItem)
		{
			//string dest = psiDestinationFolder.GetDisplayName(Sigdn.DesktopAbsoluteParsing);
			//var op = new OperationInternal {Type = "new", Destination = dest, NewName = pszNewName, HResult = hrNew};
			//_operations.Add(op);
			return SOk;
		}

		public int UpdateProgress(int iWorkTotal, int iWorkSoFar)
		{
			return SOk;
		}

		public int ResetTimer()
		{
			return SOk;
		}

		public int PauseTimer()
		{
			return SOk;
		}

		public int ResumeTimer()
		{
			return SOk;
		}
	}
}