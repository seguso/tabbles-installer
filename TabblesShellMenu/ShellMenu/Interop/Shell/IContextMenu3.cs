﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Shell
{
	/// <summary>IContextMenu Shell interface.</summary>
	/// <see cref="IContextMenu2" />
	[ComImport, Guid("BCFCE0A0-EC17-11d0-8D10-00A0C90F2719"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IContextMenu3 : IContextMenu2
	{
		[PreserveSig]
		new int QueryContextMenu(IntPtr hMenu, int indexMenu, int idCmdFirst, int idCmdLast, int flags);

		new void InvokeCommand([In] ref CmInvokeCommandInfo ici);
		new void GetCommandString(IntPtr idCmd, GetCommandStringFlags type, IntPtr reserved, IntPtr name, int chMax);

		new void HandleMenuMsg(int msg, IntPtr wParam, IntPtr lParam);

		void HandleMenuMsg2(int msg, IntPtr wParam, IntPtr lParam, IntPtr resultPtr);
	}
}